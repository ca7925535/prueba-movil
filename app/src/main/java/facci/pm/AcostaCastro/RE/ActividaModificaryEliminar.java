package facci.pm.AcostaCastro.RE;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividaModificaryEliminar extends AppCompatActivity {

    Button btn_modificar,btn_eliminar;
    EditText txt_ruta,txt_origen,txt_destino,txt_compania,txt_tiempo;

    int id;
    String ruta;
    String origen;
    String destino;
    String compania;
    String tiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activida_modificary_eliminar);

        Bundle b = getIntent().getExtras();
        if (b!=null){
            id =b.getInt("Id");
            ruta=b.getString("Ruta");
            origen=b.getString("Origen");
            destino=b.getString("Destino");
            compania=b.getString("Compania");
            tiempo=b.getString("Tiempo");
        }

        txt_ruta=findViewById(R.id.textRuta);
        txt_origen=findViewById(R.id.textOrigen);
        txt_destino=findViewById(R.id.textDestino);
        txt_compania=findViewById(R.id.textCompania);
        txt_tiempo=findViewById(R.id.textTiempo);
        btn_modificar=findViewById(R.id.btnModificar);
        btn_eliminar=findViewById(R.id.btnEliminar);

        txt_ruta.setText(ruta);
        txt_origen.setText(origen);
        txt_destino.setText(destino);
        txt_compania.setText(compania);
        txt_tiempo.setText(tiempo);

        btn_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Eliminar(id);
                onBackPressed();
            }
        });

        btn_modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(id,txt_ruta.getText().toString(),txt_origen.getText().toString(),txt_destino.getText().toString(),txt_compania.getText().toString(),txt_tiempo.getText().toString());
                onBackPressed();
            }
        });
    }

    private void Modificar(int Id,String Ruta, String Origen, String Destino, String Compania, String Tiempo){
        DbRutas rutas = new DbRutas(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();

        String sql ="update Rutas set Ruta='"+ Ruta +"',Origen='"+ Origen + "',Destino='"+ Destino + "',Compania='"+ Compania + "',Tiempo='"+ Tiempo +"' where Id="+Id;
        db.execSQL(sql);
        db.close();
    }

    private void Eliminar(int Id){
        DbRutas rutas = new DbRutas(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();

        String sql = "delete from Rutas where Id="+Id;
        db.execSQL(sql);
        db.close();
    }
}
