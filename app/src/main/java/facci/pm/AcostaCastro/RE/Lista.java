package facci.pm.AcostaCastro.RE;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Lista extends AppCompatActivity {

    ListView listViewRutas;
    ArrayList<String> listado;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        CargarListado();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listViewRutas = findViewById(R.id.listViewRutas);

        CargarListado();

        listViewRutas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Lista.this,listado.get(position),Toast.LENGTH_SHORT).show();
                int clave = Integer.parseInt(listado.get(position).split(" ")[0]);
                String ruta = listado.get(position).split(" ")[1];
                String origen = listado.get(position).split(" ")[2];
                String destino = listado.get(position).split(" ")[3];
                String compania = listado.get(position).split(" ")[4];
                String tiempo = listado.get(position).split(" ")[5];
                Intent intent = new Intent(Lista.this,ActividaModificaryEliminar.class);
                intent.putExtra("Id",clave);
                intent.putExtra("Ruta",ruta);
                intent.putExtra("Origen",origen);
                intent.putExtra("Destino",destino);
                intent.putExtra("Compania",compania);
                intent.putExtra("Tiempo",tiempo);
                startActivity(intent);
            }
        });

        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void CargarListado(){
        listado = ListaRutas();
        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        listViewRutas.setAdapter(adapter);
    }

    private ArrayList<String> ListaRutas(){
        ArrayList<String> datos = new ArrayList<String>();
        DbRutas rutas = new DbRutas(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getReadableDatabase();
        String sql ="select Id, Ruta, Origen, Destino, Compania, Tiempo from Rutas";
        Cursor c = db.rawQuery(sql,null);
        if (c.moveToFirst()){
            do {
                String linea =c.getInt(0)+" "+ c.getString(1)+" "+c.getString(2)+" "+c.getString(3)+" "+c.getString(4)+" "+c.getString(5);
                datos.add(linea);
            }while (c.moveToNext());
        }
        db.close();
        return datos;
    }
}
