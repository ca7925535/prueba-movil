package facci.pm.AcostaCastro.RE;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button btn_guardar,btn_consultar;
    EditText txt_ruta,txt_origen,txt_destino,txt_compania,txt_tiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_ruta=findViewById(R.id.textRuta);
        txt_origen=findViewById(R.id.textOrigen);
        txt_destino=findViewById(R.id.textDestino);
        txt_compania=findViewById(R.id.textCompania);
        txt_tiempo=findViewById(R.id.textTiempo);
        btn_guardar=findViewById(R.id.btnGuardar);
        btn_consultar=findViewById(R.id.btnBuscar);

        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardar(txt_ruta.getText().toString(),txt_origen.getText().toString(),txt_destino.getText().toString(),txt_compania.getText().toString(),txt_tiempo.getText().toString());
            }
        });

        btn_consultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Lista.class));
            }
        });
    }

    private void guardar(String Ruta, String Origen, String Destino, String Compania, String Tiempo){
        DbRutas rutas = new DbRutas(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();
        try {
            ContentValues   c = new ContentValues();
            c.put("Ruta",Ruta);
            c.put("Origen",Origen);
            c.put("Destino",Destino);
            c.put("Compania",Compania);
            c.put("Tiempo",Tiempo);
            db.insert("RUTAS","",c);
            db.close();
            Toast.makeText(this,"Registro insertado",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(this,"Error:" + e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}
